package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-notification.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-notification.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-notification.git/transport/grpc/proto/notification"
)

type CreateNotification struct {
	builder          *builder.Grpc
	repoNotification repo.INotification
}

func NewCreateNotification() *CreateNotification {
	return &CreateNotification{
		builder:          builder.NewGrpc(),
		repoNotification: repo.NewNotification(),
	}
}

func (h *CreateNotification) Handler(ctx context.Context, req *pb.CreateNotificationRequest) error {
	dataNotification := h.builder.CreateNotificationRequest(req)
	err := h.repoNotification.CreateUpdateNotification(ctx, dataNotification)
	if err != nil {
		return err
	}
	return nil
}
