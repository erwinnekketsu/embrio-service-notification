package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-notification.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-notification.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-notification.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-notification.git/transport/grpc/proto/notification"
	"gitlab.com/erwinnekketsu/embrio-service-notification.git/util"
)

type ListNotification struct {
	builder          *builder.Grpc
	repoNotification repo.INotification
}

func NewListNotification() *ListNotification {
	return &ListNotification{
		builder:          builder.NewGrpc(),
		repoNotification: repo.NewNotification(),
	}
}

func (h *ListNotification) Handler(ctx context.Context, req *pb.ListNotificationRequest) (res []entity.Notification, pagintaion *util.Pagination, err error) {
	return h.repoNotification.GetNotification(ctx, int(req.Id), int(req.Page), int(req.Limit), int(req.Status))
}
