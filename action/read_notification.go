package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-notification.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-notification.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-notification.git/transport/grpc/proto/notification"
)

type ReadNotification struct {
	builder          *builder.Grpc
	repoNotification repo.INotification
}

func NewReadNotification() *ReadNotification {
	return &ReadNotification{
		builder:          builder.NewGrpc(),
		repoNotification: repo.NewNotification(),
	}
}

func (h *ReadNotification) Handler(ctx context.Context, req *pb.ReadNotificationRequest) error {
	dataNotification := h.builder.ReadNotificationRequest(req)
	err := h.repoNotification.ReadNotification(ctx, dataNotification)
	if err != nil {
		return err
	}
	return nil
}
