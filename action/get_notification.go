package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-notification.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-notification.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-notification.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-notification.git/transport/grpc/proto/notification"
)

type GetNotification struct {
	builder          *builder.Grpc
	repoNotification repo.INotification
}

func NewGetNotification() *GetNotification {
	return &GetNotification{
		builder:          builder.NewGrpc(),
		repoNotification: repo.NewNotification(),
	}
}

func (h *GetNotification) Handler(ctx context.Context, req *pb.GetNotificationRequest) (res entity.Notification, err error) {
	return h.repoNotification.GetNotificationById(ctx, int(req.Id))
}
