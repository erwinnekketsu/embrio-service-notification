package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-notification.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-notification.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-notification.git/transport/grpc/proto/notification"
)

type UpdateNotification struct {
	builder          *builder.Grpc
	repoNotification repo.INotification
}

func NewUpdateNotification() *UpdateNotification {
	return &UpdateNotification{
		builder:          builder.NewGrpc(),
		repoNotification: repo.NewNotification(),
	}
}

func (h *UpdateNotification) Handler(ctx context.Context, req *pb.UpdateNotificationRequest) error {
	dataNotification := h.builder.UpdateNotificationRequest(req)
	err := h.repoNotification.CreateUpdateNotification(ctx, dataNotification)
	if err != nil {
		return err
	}
	return nil
}
