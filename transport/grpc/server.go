package grpc

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-notification.git/action"
	"gitlab.com/erwinnekketsu/embrio-service-notification.git/builder"
	pb "gitlab.com/erwinnekketsu/embrio-service-notification.git/transport/grpc/proto/notification"
)

type GrpcServer struct {
	builder *builder.Grpc
}

func (gs *GrpcServer) CreateNotification(ctx context.Context, req *pb.CreateNotificationRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewCreateNotification().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) UpdateNotification(ctx context.Context, req *pb.UpdateNotificationRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewUpdateNotification().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) ListNotification(ctx context.Context, req *pb.ListNotificationRequest) (*pb.ListNotificationResponse, error) {
	var resp pb.ListNotificationResponse
	data, pagination, err := action.NewListNotification().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	resPagination := &pb.Pagination{
		PageSize:     int64(pagination.PageSize),
		CurrentPage:  int64(pagination.CurrentPage),
		TotalPage:    int64(pagination.TotalPage),
		TotalResults: int64(pagination.TotalResult),
	}
	return &pb.ListNotificationResponse{
		Notifications: gs.builder.ListNotificationResponse(data),
		Pagination:    resPagination,
	}, nil
}

func (gs *GrpcServer) GetNotification(ctx context.Context, req *pb.GetNotificationRequest) (*pb.GetNotificationResponse, error) {
	var resp pb.GetNotificationResponse
	data, err := action.NewGetNotification().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	resp.Notifications = gs.builder.GetNotificationResponse(data)
	return &resp, nil
}

func (gs *GrpcServer) ReadNotification(ctx context.Context, req *pb.ReadNotificationRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewReadNotification().Handler(ctx, req)
	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func NewGrpcServer() *GrpcServer {
	return &GrpcServer{
		builder: builder.NewGrpc(),
	}
}
