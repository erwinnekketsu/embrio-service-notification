package redis

import (
	"context"
	"time"

	"github.com/go-redis/redis/v8"
)

// IRedis is a interface for redis
type IRedis interface {
	Get(context.Context, *redis.Client, string) (string, error)
	Set(context.Context, *redis.Client, string, interface{}, time.Duration) error
	Del(context.Context, *redis.Client, string) error
	GetBytes(context.Context, *redis.Client, interface{}, string) error
}
