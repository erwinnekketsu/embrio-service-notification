package repo

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-notification.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-notification.git/util"
)

type INotification interface {
	CreateUpdateNotification(ctx context.Context, req entity.Notification) error
	GetNotification(ctx context.Context, id int, page int, limit int, status int) (data []entity.Notification, pagintaion *util.Pagination, err error)
	GetNotificationById(ctx context.Context, id int) (data entity.Notification, err error)
	ReadNotification(ctx context.Context, req entity.Notification) error
}
