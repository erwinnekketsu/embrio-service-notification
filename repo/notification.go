package repo

import (
	"bytes"
	"context"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/jmoiron/sqlx"
	"gitlab.com/erwinnekketsu/embrio-service-notification.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-notification.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-notification.git/repo/mysql"
	"gitlab.com/erwinnekketsu/embrio-service-notification.git/util"
	"gitlab.com/erwinnekketsu/embrio-service-notification.git/util/errors"
)

type Notification struct {
	builder  *builder.Entity
	iMysql   mysql.IMysql
	embrioDB *sqlx.DB
}

func NewNotification() *Notification {
	return &Notification{
		builder:  builder.NewEntity(),
		iMysql:   mysql.NewClient(),
		embrioDB: mysql.EmbrioDB,
	}
}

func (p *Notification) CreateUpdateNotification(ctx context.Context, req entity.Notification) error {
	var query string
	if req.ID == 0 {
		query = mysql.QueryCreateNotifikasi
	} else {
		query = mysql.QueryUpdateNotifikasi
	}
	if req.From != req.To {
		go p.SendNotification(ctx, req)
	}
	dataNotification := p.builder.CreateUpdateNotification(ctx, req)
	_, err := p.iMysql.CreateOrUpdate(ctx, p.embrioDB, dataNotification, query)
	log.Println(err)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func (p *Notification) GetNotification(ctx context.Context, id int, page int, limit int, status int) (data []entity.Notification, pagintaion *util.Pagination, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"`to`$eq": id,
		},
	}

	datas, paginations, err := p.iMysql.FindWithPagination(
		ctx,
		p.embrioDB,
		query,
		util.NewPagination(int32(page), int32(limit)),
		mysql.QueryGetNotifikasi,
	)

	if err != nil {
		log.Println(err)
		return data, &util.Pagination{}, err
	}

	for _, v := range datas {
		user, err := p.GetUserById(ctx, int(v.From))
		if err != nil {
			log.Println(err)
		}
		var image string
		if user.Foto != "" {
			image = user.Foto
		} else {
			image = util.DefaultProfileImage
		}
		var date string
		log.Println(v.CreatedAt)
		if v.CreatedAt != nil {
			t := v.CreatedAt
			t.String()
			date = t.Format("2006-01-02 15:04:05")
		}
		dataNotification := entity.Notification{
			ID:             v.ID,
			ProductId:      v.ProductId,
			NotificationId: v.NotificationId,
			From:           v.From,
			To:             v.To,
			Title:          v.Title,
			Content:        v.Content,
			Date:           date,
			Image:          image,
			Name:           user.Fullname,
		}
		data = append(data, dataNotification)
	}

	return data, paginations, nil
}

func (p *Notification) GetUserById(ctx context.Context, id int) (data mysql.Nasabah, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"id$eq": id,
		},
	}
	err = p.iMysql.Get(
		ctx,
		p.embrioDB,
		&data,
		query,
		mysql.QueryGetAllNasabah,
	)

	if err != nil {
		return data, errors.ErrBadRequest("User Not Found")
	}
	return data, nil
}

func (p *Notification) GetNotificationById(ctx context.Context, id int) (data entity.Notification, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"id$eq": id,
		},
	}

	var dataNotification mysql.Notification
	err = p.iMysql.Get(
		ctx,
		p.embrioDB,
		&dataNotification,
		query,
		mysql.QueryGetNotifikasi,
	)

	if err != nil {
		log.Println(err)
		return data, err
	}

	data = p.builder.GetNotification(dataNotification)

	return data, nil
}

func (p *Notification) ReadNotification(ctx context.Context, req entity.Notification) error {
	var query string
	if req.ID == 0 {
		query = mysql.QueryUpdateStatusNotifikasiByUser
	} else {
		query = mysql.QueryUpdateStatusNotifikasiById
	}
	dataNotification := p.builder.ReadNotification(ctx, req)

	_, err := p.iMysql.CreateOrUpdate(ctx, p.embrioDB, dataNotification, query)
	log.Println(err)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

func (p *Notification) SendNotification(ctx context.Context, req entity.Notification) error {
	httpposturl := "https://fcm.googleapis.com/fcm/send"
	log.Println("HTTP JSON POST URL:", httpposturl)
	var jsonData []byte
	if req.To == 0 {
		jsonData = []byte(`{
			"notification": {
				"body": "` + req.Content + `",
				"title": "` + req.Title + `"
			},
			"data": {
				"foo": "bar"
			},
			"to": "/topics/alldevice"
		}`)

		request, error := http.NewRequest("POST", httpposturl, bytes.NewBuffer(jsonData))
		request.Header.Set("Content-Type", "application/json")
		request.Header.Set("Authorization", "key=AAAAe0oLHWg:APA91bHGkOXKaMquo_r7ynChWtdfy_y7DcPko3GkKyX_N-rTau9pGDM-r3cGW7SSyzabCNnaON3CGy29T2WO0Q_SKecEiVuJtOeuiqqW_YPLs4RVsCjPosMtgAzb5m9zxv-9Q8Dzvc32")

		client := &http.Client{}
		response, error := client.Do(request)
		if error != nil {
			panic(error)
		}
		defer response.Body.Close()

		log.Println("response Status:", response.Status)
		log.Println("response Headers:", response.Header)
		body, _ := ioutil.ReadAll(response.Body)
		log.Println("response Body:", string(body))
		return nil
	} else {
		queryToken := &util.Query{
			Filter: map[string]interface{}{
				"idNasabah$eq": req.To,
			},
		}
		dataDevice := []mysql.Devices{}
		err := p.iMysql.Select(ctx, p.embrioDB, &dataDevice, queryToken, mysql.QueryGetDevices)
		if err != nil {
			log.Println(err)
		}
		for _, v := range dataDevice {
			jsonData = []byte(`{
				"notification": {
					"body": "` + req.Content + `",
					"title": "` + req.Title + `"
				},
				"data": {
					"foo": "bar"
				},
				"to": "` + v.Token + `"
			}`)

			request, error := http.NewRequest("POST", httpposturl, bytes.NewBuffer(jsonData))
			request.Header.Set("Content-Type", "application/json")
			request.Header.Set("Authorization", "key=AAAAe0oLHWg:APA91bHGkOXKaMquo_r7ynChWtdfy_y7DcPko3GkKyX_N-rTau9pGDM-r3cGW7SSyzabCNnaON3CGy29T2WO0Q_SKecEiVuJtOeuiqqW_YPLs4RVsCjPosMtgAzb5m9zxv-9Q8Dzvc32")

			client := &http.Client{}
			response, error := client.Do(request)
			if error != nil {
				panic(error)
			}
			defer response.Body.Close()

			log.Println("response Status:", response.Status)
			log.Println("response Headers:", response.Header)
			body, _ := ioutil.ReadAll(response.Body)
			log.Println("response Body:", string(body))
			return nil
		}
		return nil
	}

}
