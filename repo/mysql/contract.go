package mysql

import (
	"context"

	"github.com/jmoiron/sqlx"
	"gitlab.com/erwinnekketsu/embrio-service-notification.git/util"
)

type IMysql interface {
	Get(context.Context, *sqlx.DB, interface{}, *util.Query, string) error
	Select(ctx context.Context, db *sqlx.DB, data interface{}, query *util.Query, queryString string) (err error)
	CreateOrUpdate(context.Context, *sqlx.DB, interface{}, string) (lastId int64, err error)
	FindWithPagination(ctx context.Context, db *sqlx.DB, query *util.Query, pagination *util.Pagination, queryString string) (data []Notification, paginate *util.Pagination, err error)
	Count(ctx context.Context, db *sqlx.DB, query *util.Query, queryString string) (int32, error)
}
