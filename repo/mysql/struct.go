package mysql

import "time"

type Notification struct {
	ID             int32      `json:"id" db:"id"`
	ProductId      int32      `json:"product_id" db:"idProduk"`
	NotificationId int32      `json:"notification_id" db:"idNotifikasi"`
	From           int32      `json:"from" db:"from"`
	To             int32      `json:"to" db:"to"`
	Title          string     `json:"title" db:"title"`
	Content        string     `json:"content" db:"content"`
	Status         int32      `json:"status" db:"status"`
	CreatedAt      *time.Time `json:"createdAt" db:"createdAt"`
}

type Devices struct {
	ID        int        `json:"id" db:"id"`
	UserId    int        `json:"user_id" db:"idNasabah"`
	Token     string     `json:"token" db:"token"`
	CreatedAt *time.Time `json:"createdAt" db:"createdAt"`
	UpdatedAt *time.Time `json:"updatedAt" db:"updatedAt"`
}

type Nasabah struct {
	ID          int    `json:"id" db:"id"`
	Email       string `json:"email" db:"email"`
	Fullname    string `json:"fullname" db:"namaLengkap"`
	Username    string `json:"username" db:"username"`
	Foto        string `json:"foto" db:"foto"`
}
