package mysql

const (
	QueryCreateNotifikasi = "INSERT INTO notifikasi (idProduk, idNotifikasi, `from`, `to`, title, content, status) VALUES (:idProduk, :idNotifikasi, :from, :to,:title, :content, :status);"

	QueryUpdateNotifikasi = "UPDATE notifikasi SET idProduk=:idProduk, idNotifikasi=:idNotifikasi, `from`=:from, `to`=:to, title=:title, content=:content, status=:status  WHERE id=:id"

	QueryGetNotifikasi = "SELECT id, idProduk, idNotifikasi, `from`, `to`, title, content, createdAt FROM notifikasi"

	QueryCountNotification = "SELECT count(1) FROM notifikasi"

	QueryUpdateStatusNotifikasiByUser = "UPDATE notifikasi SET status=:status WHERE `to`=:to"

	QueryUpdateStatusNotifikasiById = `UPDATE notifikasi SET status=:status WHERE id=:id`

	//Device
	QueryGetDevices = `
		SELECT idNasabah, token FROM devices
	`

	//Nasabah

	QueryGetAllNasabah = `
		SELECT id, username, email, namaLengkap, foto FROM nasabah
	`
)
