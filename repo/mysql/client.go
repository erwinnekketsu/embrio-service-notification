package mysql

import (
	"context"
	"log"

	"github.com/jmoiron/sqlx"
	"gitlab.com/erwinnekketsu/embrio-service-notification.git/util"
	"gitlab.com/erwinnekketsu/embrio-service-notification.git/util/errors"
)

type Client struct {
	e errors.Error
}

func NewClient() *Client {
	return &Client{}
}

func (c *Client) Get(ctx context.Context, db *sqlx.DB, data interface{}, query *util.Query, queryString string) (err error) {
	where, args := query.Where()
	q := queryString
	q += where

	if err = db.GetContext(ctx, data, db.Rebind(q), args...); err != nil {
		return
	}

	return
}

func (c *Client) Select(ctx context.Context, db *sqlx.DB, data interface{}, query *util.Query, queryString string) (err error) {
	where, args := query.Where()
	sort := query.Order()
	// limit := ` LIMIT 20 OFFSET 0`
	q := queryString
	q += where
	q += sort
	// q += limit

	if err = db.SelectContext(ctx, data, db.Rebind(q), args...); err != nil {
		return
	}

	return

}

func (m *Client) CreateOrUpdate(ctx context.Context, db *sqlx.DB, data interface{}, query string) (lastId int64, err error) {
	res, err := db.NamedExecContext(ctx, query, data)
	if err != nil {
		log.Println(err)
		return 0, err
	}
	lastId, _ = res.LastInsertId()

	return lastId, err
}

func (m *Client) FindWithPagination(ctx context.Context, db *sqlx.DB, query *util.Query, pagination *util.Pagination, queryString string) (data []Notification, paginate *util.Pagination, err error) {
	where, args := query.Where()
	sort := query.Order()
	limit := pagination.LimitOffset()
	q := queryString
	q += where
	q += sort
	q += limit

	if err = db.SelectContext(ctx, &data, db.Rebind(q), args...); err != nil {
		return
	}

	count, err := m.Count(ctx, db, query, QueryCountNotification)
	if err != nil {
		return data, &util.Pagination{}, err
	}
	return data, pagination.SetTotalPage(count), nil
}

func (m *Client) Count(ctx context.Context, db *sqlx.DB, query *util.Query, queryString string) (int32, error) {
	where, args := query.Where()
	q := queryString
	q += where
	var data int32
	if err := db.GetContext(ctx, &data, db.Rebind(q), args...); err != nil {
		return 0, m.e.ErrProcess(err.Error())
	}
	return data, nil
}
