package entity

type Notification struct {
	ID             int32  `json:"id"`
	ProductId      int32  `json:"product_id"`
	NotificationId int32  `json:"notification_id"`
	From           int32  `json:"from"`
	To             int32  `json:"to"`
	Title          string `json:"title"`
	Content        string `json:"content"`
	Status         int32  `json:"status"`
	Image          string `json:"image"`
	Date           string `json:"date"`
	Name           string `json:"name"`
}

type BodyNotificationRequest struct {
	Notification struct {
		Body  string `json:"body"`
		Title string `json:"title"`
	} `json:"notification"`
	Data struct {
		Foo string `json:"foo"`
	} `json:"data"`
	To string `json:"to"`
}
