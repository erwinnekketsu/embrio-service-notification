package builder

import (
	// "encoding/json"
	// "strconv"

	"encoding/json"

	"gitlab.com/erwinnekketsu/embrio-service-notification.git/entity"
	pb "gitlab.com/erwinnekketsu/embrio-service-notification.git/transport/grpc/proto/notification"
)

// Grpc struct grpc builder
type Grpc struct{}

// NewGrpc for initiate builder func
func NewGrpc() *Grpc {
	return &Grpc{}
}

func (g *Grpc) CreateNotificationRequest(req *pb.CreateNotificationRequest) (data entity.Notification) {
	data.ProductId = req.ProductId
	data.NotificationId = req.NotificationId
	data.From = req.From
	data.To = req.To
	data.Title = req.Title
	data.Content = req.Content
	return data
}

func (g *Grpc) ReadNotificationRequest(req *pb.ReadNotificationRequest) (data entity.Notification) {
	data.ID = req.Id
	if req.Tag == "all" {
		data.ID = 0
	}
	data.To = req.UserId
	return data
}

func (g *Grpc) UpdateNotificationRequest(req *pb.UpdateNotificationRequest) (data entity.Notification) {
	data.ID = req.Id
	data.ProductId = req.ProductId
	data.NotificationId = req.NotificationId
	data.From = req.From
	data.To = req.To
	data.Title = req.Title
	data.Content = req.Content
	return data
}

// ListNotificationResponse generate response
func (g *Grpc) ListNotificationResponse(data []entity.Notification) (res []*pb.Notifications) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}

// GetNotificationResponse generate response
func (g *Grpc) GetNotificationResponse(data entity.Notification) (res *pb.Notifications) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}
