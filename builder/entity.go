package builder

import (
	// "context"
	// "strconv"
	// "time"

	"context"
	"time"

	"github.com/cloudinary/cloudinary-go"
	"gitlab.com/erwinnekketsu/embrio-service-notification.git/entity"
	cld "gitlab.com/erwinnekketsu/embrio-service-notification.git/repo/cloudinary"
	"gitlab.com/erwinnekketsu/embrio-service-notification.git/repo/mysql"
)

type Entity struct {
	cloudinary *cloudinary.Cloudinary
}

func NewEntity() *Entity {
	return &Entity{
		cloudinary: cld.Cld,
	}
}

func (e *Entity) CreateUpdateNotification(ctx context.Context, req entity.Notification) (data mysql.Notification) {
	if req.ID != 0 {
		data.ID = req.ID
	}
	dateNow := time.Now().UTC()
	data.ProductId = req.ProductId
	data.NotificationId = req.NotificationId
	data.From = req.From
	data.To = req.To
	data.Title = req.Title
	data.Content = req.Content
	data.Status = 0
	data.CreatedAt = &dateNow
	return data
}

func (e *Entity) GetNotification(req mysql.Notification) (res entity.Notification) {
	res.ID = req.ID
	res.ProductId = req.ProductId
	res.NotificationId = req.NotificationId
	res.From = req.From
	res.To = req.To
	res.Title = req.Title
	res.Content = req.Content
	return res
}

func (e *Entity) ReadNotification(ctx context.Context, req entity.Notification) (data mysql.Notification) {
	if req.ID != 0 {
		data.ID = req.ID
	}
	data.To = req.To
	data.Status = 1
	return data
}
